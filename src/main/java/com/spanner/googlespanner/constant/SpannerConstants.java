package com.spanner.googlespanner.constant;

public class SpannerConstants {

    public static final String TABLE_NAME = "table_name";
    public static final String DATABASE_NAME = "database_name";
    public static final String OUTBOX_TABLE_NAME = "OUTBOX";
    public static final String ANY_API_V1 = "/v1/api/get";
    public static final String OUTBOX_API_V1 = "/v1/outbox/api/get";
    public static final String INBOX_TABLE_NAME = "INBOX";
    public static final String SAMPLE_TEST_BOX_TABLE_NAME = "SAMPLE_TEST_BOX";
    public static final String INBOX_API_V1 = "/v1/inbox/api/get";
    public static final String DATABASE_ONE = "database-1";
    public static final String DATABASE_TWO = "database-2";
    public static final String DATABASE_FOUR = "database-4";
}
