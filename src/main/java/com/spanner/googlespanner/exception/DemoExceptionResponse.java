package com.spanner.googlespanner.exception;

import org.springframework.web.reactive.function.client.WebClientResponseException;

public class DemoExceptionResponse extends Exception{

    public DemoExceptionResponse(String message, WebClientResponseException ex) {
        super(message, ex);
    }

    public DemoExceptionResponse(Exception ex) {
        super(ex);
    }
}
