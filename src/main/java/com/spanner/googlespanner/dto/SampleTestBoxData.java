package com.spanner.googlespanner.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SampleTestBoxData {

    @JsonProperty("locator")
    private String parentLoc;

    @JsonProperty("created")
    private Date created;

    @JsonProperty("sample_data")
    private String sampleData;

    @JsonProperty("status")
    private Integer status;

    @JsonProperty("version")
    private Date version;
}
