package com.spanner.googlespanner.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class InboxData {

    @JsonProperty("locator")
    private String parentLoc;

    @JsonProperty("created")
    private Date created;

    @JsonProperty("data")
    private String data;

    @JsonProperty("status")
    private Integer status;

    @JsonProperty("retry_count")
    private Integer retryCount;

    @JsonProperty("version")
    private Date updated;
}
