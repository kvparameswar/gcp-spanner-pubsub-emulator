package com.spanner.googlespanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
public class GoogleSpannerApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoogleSpannerApplication.class, args);
	}

}
